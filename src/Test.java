import Money.Currency;
import Money.Locale;
import Money.Money;
import Money.ExchangeRate;
import Money.CurrencyConverter;
import Quantity.Quantity;
import Quantity.Real;
import org.junit.Before;

import javax.management.loading.PrivateClassLoader;
import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Mirjam on 21.12.2016.
 */

public class Test {
    Currency eur;
    Locale locale1;
    Money myPocketMoney;
    ArrayList testList;
    Quantity q;
    Real r;

    @Before
    public void setUp() {
        eur = new Currency("Euro", "EUR", "€");
        locale1 = new Locale("EST", "Estonia");
        eur.addLocale(locale1);
        myPocketMoney = new Money(new Real(new BigDecimal(100)), eur);
        testList = new ArrayList();
        r = new Real(new BigDecimal(100));
        q = new Quantity(r, eur);
    }
    @org.junit.Test
    public void testGetName() throws Exception {
        assertEquals(eur.getName(), "Euro");
    }

    @org.junit.Test
    public void testGetSymbol() throws Exception {
        assertEquals(eur.getSymbol(), "€");
    }

    @org.junit.Test
    public void testGetDefinition() throws Exception {
        //implementeerimata meetod
    }

    @org.junit.Test
    public void testGetLocales() throws Exception {
        assertEquals("Empty list should have 0 elements", 0, testList.size());
    }

    @org.junit.Test
    public void testAddLocale() throws Exception {
        testList.add(1);
        assertEquals( 1, testList.size());
    }

    @org.junit.Test
    public void testGetCurrency() throws Exception {
        assertEquals(eur, myPocketMoney.getCurrency());
    }

    @org.junit.Test
    public void testEqualTo() throws Exception {
        assertEquals(q.getAmount().getValue(), myPocketMoney.getAmount().getValue());
    }

    @org.junit.Test
    public void testGreaterThan() throws Exception {
        Quantity q1 = new Quantity(new Real(new BigDecimal(101)), eur);
        assertFalse(myPocketMoney.greaterThan(q1));
    }

    @org.junit.Test
    public void testLessThan() throws Exception {
        Quantity q1 = new Quantity(new Real(new BigDecimal(101)), eur);
        assertFalse(myPocketMoney.lessThan(q1));
    }

    @org.junit.Test
    public void testAdd(){
        myPocketMoney.add(new Money(new Real(new BigDecimal(25)), eur));
        assertEquals(new BigDecimal(125), myPocketMoney.getAmount().getValue());
    }


    @org.junit.Test
    public void testGetLocaleIndentifier(){
        assertEquals("EST", eur.getLocales().get(0).getIdentifier());
    }

    @org.junit.Test
    public void testSetIndentifier(){
        Locale locale2 = new Locale("LAT", "Latvia");
        eur.addLocale(locale2);
        assertEquals("LAT", eur.getLocales().get(1).getIdentifier());
    }

    @org.junit.Test
    public void testSetDescription(){
        locale1.setDescription("Põhjapoolsem Balti riik");
        assertEquals("Põhjapoolsem Balti riik", locale1.getDescription());
    }

    @org.junit.Test
    public void testGetDescription(){
        locale1.setDescription("Balti riik");
        assertEquals("Balti riik", locale1.getDescription());
    }

    @org.junit.Test
    public void testExchangeCurrency() {
        Money testMoney = new Money(new Real(new BigDecimal(100)), eur);

        Currency dollar = new Currency("Dollar", "USD", "$");

        ExchangeRate eurDollarRate = new ExchangeRate(eur, dollar, new Real(new BigDecimal(1.1)));
        CurrencyConverter converter = new CurrencyConverter();
        converter.addExchangeRate(eurDollarRate);

        Money exchangedPocketMoney = converter.exchange(myPocketMoney, dollar, eurDollarRate);

        //assert correct currency exhange with max error delta
        BigDecimal delta = new BigDecimal(0.001);
        assertTrue(exchangedPocketMoney.getAmount().getValue().add(new BigDecimal(110).negate()).compareTo(delta) < 0);
    }

}
