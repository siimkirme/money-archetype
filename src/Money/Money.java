package Money;

import Quantity.Quantity;
import Quantity.Real;

import java.math.BigDecimal;

/**
 * Created by Siim on 2016-12-19.
 */
public class Money extends Quantity {
    public Currency getCurrency() {
        return (Currency) super.getMetric();
    }

    @Override
    public boolean equalTo(Quantity quantity) {
        if (!legalOperation(quantity)) {
            return false;
        }

        return quantity.getAmount().getValue().equals(this.getAmount().getValue());
    }

    @Override
    public boolean greaterThan(Quantity quantity) {
        if (!legalOperation(quantity)) {
            return false;
        }

        return quantity.getAmount().getValue().compareTo(this.getAmount().getValue()) > 0;
    }

    @Override
    public boolean lessThan(Quantity quantity) {
        if (!legalOperation(quantity)) {
            return false;
        }

        return quantity.getAmount().getValue().compareTo(this.getAmount().getValue()) < 0;
    }

    @Override
    public Quantity add(Quantity quantity) {
        if (!legalOperation(quantity)) {
            return null;//TODO
        }

        this.getAmount().add(quantity.getAmount().getValue());

        return this;
    }

    @Override
    public Quantity substract(Quantity quantity) {
        if (!legalOperation(quantity)) {
            return null;//TODO
        }

        this.getAmount().add(quantity.getAmount().getValue().negate());

        return this;
    }

    private boolean legalOperation(Quantity quantity) {
        if (!(quantity instanceof Money)) {
            return false;//TODO
        }
        if (!currencyEquals(quantity)) {
            return false;
        }
        return true;
    }

    private boolean currencyEquals(Quantity quantity) {
        if (!(quantity instanceof Money)) {
            return false;
        }
        return ((Money) quantity).getCurrency().equals(this.getCurrency());
    }

    public Money(Real amount, Currency currency) {
        super(amount, currency);
    }

    public static void main(String[] args) {
        //create currency:
        Currency euro = new Currency("Euro", "EUR", "€");

        //add the locations its used in:
        euro.addLocale(new Locale("EST", "Estonia"));
        euro.addLocale(new Locale("LAT", "Latvia"));
        euro.addLocale(new Locale("LIT", "Lithuania"));

        //create money in my pocket:
        Money myPocketMoney = new Money(new Real(new BigDecimal(100)), euro);

        //add 25 euros to my pocket:
        myPocketMoney.add(new Money(new Real(new BigDecimal(25)), euro));

        System.out.println("Amount:" + myPocketMoney.getAmount().getValue());
        System.out.println("Currency:" + myPocketMoney.getCurrency().getName());

        //add another currency:
        Currency dollar = new Currency("Dollar", "USD", "$");

        //add an exchange rate between euro and dollar:
        ExchangeRate eurDollarRate = new ExchangeRate(euro, dollar, new Real(new BigDecimal(1.03655)));
        CurrencyConverter converter = new CurrencyConverter();
        converter.addExchangeRate(eurDollarRate);

        //exchange myPocketMoney to dollar:
        Money exchangedPocketMoney = converter.exchange(myPocketMoney, dollar, eurDollarRate);

        System.out.println("Amount:" + exchangedPocketMoney.getAmount().getValue());
        System.out.println("Currency:" + exchangedPocketMoney.getCurrency().getName());
    }
}
