package Money;

import Quantity.Real;

import java.util.Date;

/**
 * Created by Siim on 2016-12-20.
 */
public class ExchangeRate {
    private Currency from;
    private Currency to;
    private Real rate;
    private Date validFrom;
    private Date validTo;

    public ExchangeRate(Currency from, Currency to, Real rate) {
        this.from = from;
        this.to = to;
        this.rate = rate;
    }

    public Real getRate() {
        return rate;
    }
}
