package Money;

import Quantity.Metric;
import Quantity.Real;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Siim on 2016-12-19.
 */
public class Currency extends Metric {
    private String name;
    private String definition;
    private String alphapeticCode;
    private String numericCode;
    private String majorUnitSymbol;
    private String minorUnitSymbol;
    private Real ratioOfMinorUnitToMajorUnit;
    private Date introductionDate;
    private Date expirationDate;

    private ArrayList<Locale> locales;

    public Currency(String name, String alphapeticCode, String majorUnitSymbol) {
        this.name = name;
        this.alphapeticCode = alphapeticCode;
        this.majorUnitSymbol = majorUnitSymbol;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSymbol() {
        return majorUnitSymbol;
    }

    @Override
    public String getDefinition() {
        return definition;
    }

    public ArrayList<Locale> getLocales() {
        return locales;
    }

    public void addLocale(Locale locale) {
        if (locales == null) {
            locales = new ArrayList<Locale>();
        }

        locales.add(locale);
    }

}
