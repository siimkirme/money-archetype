package Money;

import Quantity.Real;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Siim on 2016-12-20.
 */
public class CurrencyConverter {
    private ArrayList<ExchangeRate> exchangeRates;

    public CurrencyConverter() {
        exchangeRates = new ArrayList<ExchangeRate>();
    }

    public Money exchange(Money amount, Currency  to, ExchangeRate rate) {
        Money exchangedMoney = new Money(new Real(new BigDecimal(amount.getAmount().getValue().toString())), to);

        exchangedMoney.multiply(rate.getRate());

        return exchangedMoney;
    }
    public void addExchangeRate(ExchangeRate rate) {
        exchangeRates.add(rate);
    }
    public ArrayList<ExchangeRate> getExchangeRates(Currency from, Currency to, Date dateFrom, Date dateTo) {
        return exchangeRates;//TODO: search
    }
}
