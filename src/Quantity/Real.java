package Quantity;

import java.math.BigDecimal;

/**
 * Created by Siim on 2016-12-19.
 */
public class Real {
    private BigDecimal value;

    public Real(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal  getValue() {
        return value;
    }

    public void setValue(BigDecimal  amount) {
        this.value = amount;
    }

    public void add(BigDecimal  amount) {
        this.value =  value.add(amount);
    }

    public void add(Real  amount) {
        this.value =  value.add(amount.getValue());
    }

    public void multiply(Real multiplier) {
        value = value.multiply(multiplier.getValue());
    }

    public void divide(Real divisor) {
        value.divide(divisor.getValue());
    }
}
