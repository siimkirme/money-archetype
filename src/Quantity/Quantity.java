package Quantity;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by Siim on 2016-12-19.
 */
public class Quantity {

    private Real amount;
    private Metric metric;

    public Quantity(Real amount, Metric metric) {
        this.amount = amount;
        this.metric = metric;
    }

    public Real getAmount() {
        return amount;
    }

    public Metric getMetric() {
        return metric;
    }

    public Quantity add(Quantity quantity) {
        amount.add(quantity.getAmount());
        return this;
    }

    public Quantity substract(Quantity quantity) {
        amount.add(quantity.getAmount().getValue().negate());
        return this;
    }

    public Quantity divide(Real divider) {
        getAmount().divide(divider);
        return this;
    }

    public Quantity multiply(Real multiplier) {
        getAmount().multiply(multiplier);
        return this;
    }

    public Quantity multiply(Quantity quantity) {
        amount.multiply(quantity.getAmount());
        return  this;
    }

    public Quantity divide(Quantity quantity) {
        amount.divide(quantity.getAmount());
        return this;
    }

    public Quantity round(RoundingPolicy policy) {
        throw new NotImplementedException();
    }

    public boolean equalTo(Quantity quantity) {
        return this.metric.equals(quantity.metric)
                && this.getAmount().getValue().compareTo(quantity.getAmount().getValue()) == 0;
    }

    public boolean greaterThan(Quantity quantity) {
        return this.getAmount().getValue().compareTo(quantity.getAmount().getValue()) > 0;
    }

    public  boolean lessThan(Quantity quantity) {
        return this.getAmount().getValue().compareTo(quantity.getAmount().getValue()) < 0;
    }
}
