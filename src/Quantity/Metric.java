package Quantity;

/**
 * Created by Siim on 2016-12-19.
 */
public abstract class Metric {
    public abstract String getName();
    public abstract String getSymbol();
    public abstract String getDefinition();
}
